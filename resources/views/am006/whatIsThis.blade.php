<!-- Stored in resources/views/am006/indice.blade.php -->

@extends('am006.main')

@section('content')
    <p>
        Afinal, voc� sabe o que &eacute; Cidadania?
        Para n�o lhe cansar com discurso enfadonho e filos&oacute;fico sobre o tema,
        recorri ao recurso mais atual para tirar nossas d&uacute;vidas: <strong>tio Google&reg;.</strong>
    </p>
    <img src="http://www.pioneirosdacolina.com.br/assets/images/missionariasEComunitarias/cidadaniaCrista/definition.png" class="rounded mx-auto d-block" style="width: 600px" />
    <p>
        Como vemos acima, cidadania implica em <strong>DIREITOS</strong>. E quando falo em direitos, uso o termo vinculado &agrave; lei.  Em outras palavras,
        a cidadania garante a pessoa f�sica os direitos estabelecidos em lei para todo o nacional de um pa&iacute;s.
        Mas n&atilde;o s&oacute;, a ele tamb&eacute;m &eacute; atribu&iacute;do os deveres.
    </p>
    <p>Ok. Acho que entendi. Mas esse neg&oacute;cio de leis � muito complicado. Pra ganhar essa especialidade eu preciso fazer uma faculdade de direito?</p>
    <h1>Calma filhote!!!!</h1>
    <p>
        Voc&ecirc; n&atilde;o precisa ser <strong>expert</strong> nos diversos c&oacute;digos de leis.
        Mas, como cidad&atilde;o, &eacute; importante que tenha familiaridade ao menos com o principal conjunto de leis: a Constitui&ccedil;&atilde;o Federal.
    </p>
@endsection

