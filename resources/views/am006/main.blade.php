@extends('honor')

@section('title', 'Atividades Missionárias e Comunitárias - Cidadania Cristã - ' . $current["title"] )
@section('cardTitle', $current["title"])

@if( isset( $previous ) && !empty( $previous ) && $previous["view"] != "indice" )
    @section('linkIndex', '/AtividadesMissionarias/AM006CidadaniaCrista')
    @section('indexPage', '&Iacute;ndice')
@endif


@if( isset( $previous ) && !empty( $previous ) )
    @section('linkPrevious', $routeRoot . $previous["route"] )
    @section('previousPage', '&lt; ' . $previous["title"])
@endif

@if( isset( $next ) && !empty( $next ) )
    @section('linkNext', $routeRoot . $next["route"])
    @section('nextPage', $next["title"] . ' &gt;')
@endif

@section('header')
    <div class="card-header text-center">
        <h1 class="title">AM006 - Cidadania Cristã <img src="http://mda.wiki.br/@imgs_wiki/imagem@AM006.png?20181021013805"></h1>
    </div>
@endsection

@section('footer')
    <div class="card-footer text-muted text-center">
        <div class="row">
            <div class="col-sm">
                <a href="@yield('linkPrevious')">@yield('previousPage')</a>
            </div>
            <div class="col-sm">
                <a href="@yield('linkIndex')">@yield('indexPage')</a>
            </div>
            <div class="col-sm">
                <a href="@yield('linkNext')">@yield('nextPage')</a>
            </div>
        </div>
    </div>
@endsection
