<!-- Stored in resources/views/am006/indice.blade.php -->

@extends('am006.main')

@section('content')
    <ul class="list-unstyled">
        <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/BoasVindas">Boas vindas</a> </li>
        <li>
            <a href="/AtividadesMissionarias/AM006CidadaniaCrista/QueECidadania">O que &eacute; cidadania?</a>
            <ul>
                <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal">Constitui&ccedil;&atilde;o Federal</a> </li>
                <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/DireitosEDeveres">Direitos e Deveres</a></li>
                <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/Nacionalidade">Nacionalidade</a></li>
                <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/FormaDeGoverno">Forma de Governo</a></li>
            </ul>
        </li>
    </ul>
@endsection

