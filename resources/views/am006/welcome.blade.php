<!-- Stored in resources/views/am006/indice.blade.php -->

@extends('am006.main')

@section('content')
    <p>
        Maranata! &Eacute; sempre bom cumprir mais uma especialidade, independente se a faz como requisito de uma classe
        ou somente para aumentar a quantidade de ins&iacute;gnias em sua faixa.
    </p>
    <p>
        Eu mesmo, gosto de fazer especialidades pelo fato de abrir a minha mente. Sim, toda especialidade é um conhecimento
        novo que agrego e, com isso, fico mais inteligente e esperto.
    </p>
    <p>
        Na pequena apostila que se segue, queremos lhe ajudar no cumprimento da especialidade de Cidadania Crist&atilde;.
        Logo, veremos temas como:
        <ul>
            <li>O que &eacute; Cidadania</li>
            <li>Conheceremos um pouco a Constituii&ccedil;&atilde;o Federal</li>
            <li>Diferença entre Cidadania e Residência Permanente</li>
            <li>Direitos e deveres dos cidadãos.</li>
        </ul>
    </p>
    <p></p>
    <p>É importante lembrar que, como ser humano, erros podem acontecer. Caso queira reportar algum erro encontrado entre em contato pelos e-mails:
        <a href="mailto:mayck.cipriano@gmail.com">mayck.cipriano@gmail.com</a> e <a href="mailto:universidade@pioneirosdacolina.com.br">universidade@pioneirosdacolina.com.br</a>.
        Para verificar as erratas ou baixar a versão mais atual dessa apostila acesse:</p>
    <p>
        •	<a href="https://goo.gl/savbnR">http://universidade.pioneirosdacolina.com.br/AtividadesMissionarias/AM006CidadaniaCrista</a><br />
        ou<br />
        •	<a href="https://goo.gl/savbnR">https://goo.gl/savbnR</a> (Criar link resumido)
    </p>

@endsection

