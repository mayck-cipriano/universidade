<!-- Stored in resources/views/am006/indice.blade.php -->

@extends('am006.main')

@section('content')
    <p>
        Antes de mais nada, voc&ecirc; sabia que toda e qualquer lei &eacute; dispon&iacute;vel a todos pela internet?
        No caso da Constituição, você pode acessar nesse link <a href="https://goo.gl/jBkSbH" target="_blank">https://goo.gl/jBkSbH</a>.
    </p>
    <h4>O que &eacute; a Constitui&ccedil;&atilde;o Federal?</h4>
    <blockquote class="blockquote text-center">
        <p class="mb-0">
            "O documento é um conjunto de regras de governo que rege o ordenamento jurídico de um País.
            A versão em vigor atualmente -- a sétima na história do Brasil-- foi promulgada em 5 de outubro de 1988.
            O texto marcou o processo de redemocratização após período de regime militar (1964 a 1985)."
        </p>
        <footer class="blockquote-footer"><cite title="A Constitui&ccedil;&atilde;o Federal">A Constitui&ccedil;&atilde;o Federal<a href="#nota1">&sup1;</a></cite></footer>
    </blockquote>

    <p>
        Esse documento descreve com detalhes os direitos e deveres dos cidad&atilde;os brasileiros. Atualmente, possui 250 artigos divididos entre 9 T&iacute;tulos.
    </p>
    <p>
        O artigo 60 permite altera&ccedil;&otilde;es a mesma, por meio de emendas. Desde a sua ado&ccedil;&atilde;o em 1988, já foram mais de 80 emendas<a href="#nota2">&sup2;</a>.
        Por&eacute;m, no mesmo artigo, é determinado aquilo que n&atilde;o pode ser alterado na mesma. Essas recebem o nome de <i>Cl&aacute;usulas P&eacute;treas</i>.
        Entre essas est&atilde;o: a separa&ccedil;&atilde;o dos poderes, o sistema federativo do Estado, direitos e garantias individuais, etc.
    </p>
    <p>
        Somente para compara&ccedil;&atilde;o, a Constitui&ccedil;&atilde;o Americana possui apenas 7 e 27 emendas<a href="#nota3">&sup3;</a>
    </p>
    <p>
        Para o cumprimento dessa especialidade, iremos estudar um pouco mais sobre:
        <ul>
            <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/DireitosEDeveres">Direitos e Deveres</a></li>
            <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/Nacionalidade">Nacionalidade</a></li>
            <li><a href="/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/FormaDeGoverno">Forma de Governo</a></li>
        </ul>
    </p>
    <hr />
    <p>
        &sup1;<a href="http://www2.planalto.gov.br/conheca-a-presidencia/acervo/constituicao-federal" id="nota1" target="_blank">A Constitui&ccedil;&atilde;o Federal (http://www2.planalto.gov.br/conheca-a-presidencia/acervo/constituicao-federal)</a><br />
        &sup2;<a href="http://www.stf.jus.br/portal/cms/verNoticiaDetalhe.asp?idConteudo=250119" id="nota2" target="_blank">O STF e os 25 Anos da Constituição(http://www.stf.jus.br/portal/cms/verNoticiaDetalhe.asp?idConteudo=250119)</a><br />
        &sup3;<a href="https://pt.wikipedia.org/wiki/Constitui%C3%A7%C3%A3o_dos_Estados_Unidos" id="nota3" target="_blank">O STF e os 25 Anos da Constituição(https://pt.wikipedia.org/wiki/Constitui%C3%A7%C3%A3o_dos_Estados_Unidos)</a><br />
    </p>
@endsection

