<?php
/**
 * Created by PhpStorm.
 * User: vo
 * Date: 22/10/2018
 * Time: 01:08
 */
use App\Constants\Honors\CidadaniaCrista;

Route::get('/AtividadesMissionarias/{hornor}/{topic?}/{subtopic?}',
    function ( $hornor, $topic = "indice", $subtopic = "" ) {
        if( empty( $hornor ) ){
            abort(404);
        }

        $hornorConstatns = null;

        if( $hornor == "AM006CidadaniaCrista" ){
            $hornorConstatns = new CidadaniaCrista();
        }

        if( !empty( $subtopic ) ){
            $topic = $subtopic;
        }

        $constants = $hornorConstatns::getConstants( $topic );

        $view = $hornorConstatns::$FOLDER_VIEW . "." . $constants["current"]["view"];
        return view($view, $constants);
    });
/*
Route::get('/AtividadesMissionarias/AM006CidadaniaCrista/BoasVindas', function () {
    return view('am006.welcome');
});

Route::get('/AtividadesMissionarias/AM006CidadaniaCrista/QueECidadania', function () {
    return view('am006.whatIsThis');
});

Route::get('/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal', function () {
    return view('am006.ConstituicaoFederal');
});

Route::get('/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/DireitosEDeveres', function () {
    return view('am006.direitosDeveres');
});

Route::get('/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/Nacionalidade', function () {
    return view('am006.nacionalidade');
});

Route::get('/AtividadesMissionarias/AM006CidadaniaCrista/ConstituicaoFederal/FormaDeGoverno', function () {
    return view('am006.formaDeGoverno');
});
*/