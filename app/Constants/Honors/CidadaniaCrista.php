<?php
namespace App\Constants\Honors;

class CidadaniaCrista
{
    public static $FIRST = "indice";
    public static $ROUTE_ROOT = "/AtividadesMissionarias/AM006CidadaniaCrista";
    public static $FOLDER_VIEW = "am006";
    public static $TOPICOS = array(
        "indice" => array(
            "view" => "indice",
            "title" => "Índice",
            "next"  => "BoasVindas",
            "previous" => "",
            "route" => ""
        ),
        "BoasVindas" => array(
            "view" => "welcome",
            "title" => "Boas Vindas",
            "next"  => "QueECidadania",
            "previous" => "indice",
            "route" => "/BoasVindas"
        ),
        "QueECidadania" => array(
            "view" => "whatIsThis",
            "title" => "O que é Cidadania?",
            "next"  => "ConstituicaoFederal",
            "previous" => "BoasVindas",
            "route" => "/QueECidadania"
        ),
        "ConstituicaoFederal" => array(
            "view" => "ConstituicaoFederal",
            "title" => "Constituição Federal",
            "next"  => "DireitosEDeveres",
            "previous" => "QueECidadania",
            "route" => "/ConstituicaoFederal"
        ),
        "DireitosEDeveres" => array(
            "view" => "direitosDeveres",
            "title" => "Direitos e Deveres",
            "next"  => "Nacionalidade",
            "previous" => "ConstituicaoFederal",
            "route" => "/ConstituicaoFederal/DireitosEDeveres"
        ),
        "Nacionalidade" => array(
            "view" => "nacionalidade",
            "title" => "Nacionalidade",
            "next"  => "FormaDeGoverno",
            "previous" => "DireitosEDeveres",
            "route" => "/ConstituicaoFederal/Nacionalidade"
        ),
        "FormaDeGoverno" => array(
            "view" => "formaDeGoverno",
            "title" => "Forma de Governo",
            "next"  => "",
            "previous" => "Nacionalidade",
            "route" => "/ConstituicaoFederal/FormaDeGoverno"
        ),
    );

    public static function getConstants( $index ){
        $current = CidadaniaCrista::getItem( $index );
        $previous = CidadaniaCrista::getItem( $current["previous"] );
        $next = CidadaniaCrista::getItem( $current["next"] );

        return array(
          "routeRoot" => CidadaniaCrista::$ROUTE_ROOT,
          "current" =>  $current,
          "previous" => $previous,
          "next" => $next
        );
    }

    public static function getItem( $key ){
        return CidadaniaCrista::isItemValid( $key )
            ? CidadaniaCrista::$TOPICOS[ $key ]
            : array();
    }

    public static function isItemValid( $key ){
        return !empty( $key ) && isset( $key )
            && isset( CidadaniaCrista::$TOPICOS[ $key ] );
    }
}